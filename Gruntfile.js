/*
* Gruntfile.js
* @author Alex Seman
*/

var LIVERELOAD_PORT = 35729,
	SERVING_PORT = 1337,
	lrInstance = require('connect-livereload')({
		port: LIVERELOAD_PORT,
		ignore: ['.js', '.svg']
	}),
	mountFolder = function(connect, dir) {
		return connect.static(require('path').resolve(dir));
	},
	now = Date.now();

/*global module:false*/
module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		// Metadata.
		pkg: grunt.file.readJSON('package.json'),
		banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
			'<%= grunt.template.today("yyyy-mm-dd") %>\n' +
			'<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
			' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %> */\n',
		jsFilename: 'build',
		cssFilename: 'build',
		now: now,
		// LiveReload setup
		connect: {
			options: {
				port: SERVING_PORT,
				hostname: 'localhost'
			},
			dev: {
				options: {
					middleware: function(connect) {
						return [lrInstance, mountFolder(connect, '.')];
					}
				}
			}
		},
		// Launch a server and open the browser
		open: {
			server: { path: 'http://<%= connect.options.hostname + ":" + connect.options.port %>' }
		},
		// Task configuration
		clean: {
			cssjs: [
				'css/<%= cssFilename %>*.css',
				'js/<%= jsFilename %>*.js'
			],
			img: [
				'img/*.{png,jpg,gif}'
			]
		},
		replace: {
			dev: {
				options: {
					patterns: [
						{
							match: '/cssBuildFile[^]*<link .*\\s?\/?>$/m',
							replacement: 'cssBuildFile\n\t\t<link rel="stylesheet" href="css/<%= cssFilename + now %>.css" type="text/css" media="screen, projection, tv" />',
							expression: true
						},
						{
							match: '/jsBuildFile[^]*<[^>]*script>/m',
							replacement: 'jsBuildFile\n\t\t<script src="js/<%= jsFilename + now %>.js"></script>',
							expression: true
						}
					]
				},
				files: [{
					'src': 'index.html',
					'dest': 'index.html'
				}]
			}
		},
		compass: {
			dev: {
				options: {
					config: 'config.rb'
				}
			}
		},
		csslint: {
			dev: {
				options: {
					csslintrc: '.csslintrc'
				},
				src: [
					'css/*.css',
					'!css/normalize.css',
					'!css/<%= cssFilename %>*.css'
				]
			}
		},
		cssmin: {
			dev: {
				options: {
					banner: '<%= banner %>'
				},
				files: {
					'css/<%= cssFilename + now %>.css': ['css/normalize.css', 'css/screen.css']
				}
			}
		},
		concat: {
			options: {
				banner: '<%= banner %>',
				stripBanners: true,
				process: function(src, filepath) {
					return '// Source: ' + filepath + '\n' + src.replace(/('use strict'|"use strict");?/g, '');
				}
			},
			dev: {
				src: [
					'lib/angular/angular.js',
					'lib/es5-shim/es5-shim.js',
					'lib/es6-shim/es6-shim.js',
					'js/**/*.js',
					'!js/<%= jsFilename %>*.js'
				],
				dest: 'js/<%= jsFilename + now %>.js'
			}
		},
		uglify: {
			options: {
				banner: '<%= banner %>'
			},
			dev: {
				src: 'js/<%= jsFilename + now %>.js',
				dest: 'js/<%= jsFilename + now %>.js'
			}
		},
		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				immed: true,
				latedef: true,
				newcap: true,
				noarg: true,
				sub: true,
				undef: false,
				unused: false,
				boss: true,
				eqnull: true,
				browser: true,
				globals: {
					angular: true,
					jQuery: true
				}
			},
			gruntfile: {
				src: 'Gruntfile.js'
			},
			lib_test: {
				src: ['js/**/*.js', '!js/<%= jsFilename %>*.js']
			}
		},
		imagemin: {
			dev: {
				options: {
					progressive: true,
				},
				files: [{
					expand: true,
					cwd: 'img/',
					src: ['**/*.{png,jpg,gif}', '!min/**/*.{png,jpg,gif}'],
					dest: 'img/min'
				}]
			}
		},
		watch: {
			jsWatch: {
				files: [
					'js/**/*.js',
					'!js/<%= jsFilename %>*.js'
				],
				tasks: ['jshint']
			},
			sassWatch: {
				options: {
					livereload: LIVERELOAD_PORT
				},
				files: [
					'sass/**/*.scss'
				],
				tasks: ['compass']
			},
			cssWatch: {
				options: {
					livereload: LIVERELOAD_PORT
				},
				files: [
					'css/*.css', 
					'!css/normalize.css',
					'!css/<%= cssFilename %>*.css'
				],
				tasks: ['csslint']
			},
			htmlWatch: {
				options: {
					livereload: LIVERELOAD_PORT
				},
				files: [
					'**/*.{html, md}'
				]
			},
			imagesWatch: {
				options: {
					livereload: LIVERELOAD_PORT
				},
				files: [
					'img/**/*.{jpg,png,jpeg,JPG}'
				]
			}
		}
	});

	grunt.event.on('watch', function(action, filepath, target) {
		grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
	});

	// Load deps:
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Tasks:
	grunt.registerTask('default', ['compass', 'csslint', 'jshint', 'connect:dev', 'open', 'watch']);
	grunt.registerTask('build', ['clean:cssjs', 'cssmin', 'concat', 'uglify', 'imagemin', 'clean:img', 'replace']);
};
