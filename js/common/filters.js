/* Filters */

angular.module('myApp.linkTargetFilter', []).
	filter('linkTarget', function() {
		return function(input) {
			return input ? '_blank' : '_self';
		};
	});

