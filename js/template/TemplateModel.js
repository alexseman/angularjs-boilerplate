(function(angular) {
	'use strict';
	
	var templateModule = angular.module('myApp.templateModel', []),
		TemplateModel = function TemplateModel() {
			return {
				create: function() {
				}
			};
		};

	templateModule.factory('templateModel', function() {
		return new TemplateModel();
	});
}(angular));
