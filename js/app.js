// Declare app level module which depends on filters, and services
// angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers']).

angular.module('myApp', [
	]).
	config(['$routeProvider', function($routeProvider) {
		$routeProvider.
			when('/', {
				templateUrl: '',
				controller: ''
			}).
			otherwise({
				redirectTo: '/'
			});
	}]).
	config(['$locationProvider', function($locationProvider) {
		$locationProvider.html5Mode(true);
	}]);

