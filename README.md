# AngularJS Boilerplate

AngularJS boilerplate project with much of the workflow based on [Grunt](http://gruntjs.com).
The directory structure and code organization is mainly a combination between the [angular-seed](https://github.com/angular/angular-seed) 
project and Cliff Meyers' [code organization in AngularJS projects](http://cliffmeyers.com/blog/2013/4/21/code-organization-angularjs-javascript). A test suite is not included.

There are two Grunt tasks configured:

1. __grunt default__ witch puts Grunt in a "watch" state running the following:
	* [LiveReload](http://livereload.com/) server
	* [Compass](http://compass-style.org/)
	* [CSSLint](http://csslint.net)
	* [JSHint](http://jshint.com)


2. __grunt build__ for when your project is production ready. It will:
	* [Minify and concatenate](https://npmjs.org/package/grunt-contrib-cssmin) your CSS files into a single one
	* [Concatenate](https://npmjs.org/package/grunt-contrib-concat) and [Uglify](https://npmjs.org/package/grunt-contrib-uglify) your JS files
	* [Optimize](https://npmjs.org/package/grunt-contrib-imagemin) your images
	* Make use of version revving for your build CSS & JS files and place their respective tags in designated (and commented out) portions of your "index.html"

__Included development libraries:__

* Main [AngularJS](https://github.com/angular/angular.js) library
* [angular-resource](https://github.com/angular/angular.js/tree/master/src/ngResource) module
* [es5-shim](https://github.com/kriskowal/es5-shim)
* [es6-shim](https://github.com/paulmillr/es6-shim)
* [html5shiv](https://github.com/aFarkas/html5shiv)
 

## Installation

First you need to have `NodeJS` with `npm`, `GruntJS` and `Twitter Bower` installed. If youre're not familiar to any of them
check out [this guide](https://www.youtube.com/watch?v=vkRv0r_tNXY) first. Also you need to have `SASS` with `Compass` installed and if that's not the case, check [this guide](http://compass-style.org/help/) also.


Then:

1. Clone the repository:

		git clone https://alexseman@bitbucket.org/alexseman/angularjs-boilerplate.git

2. `cd angularjs-boilerplate`

3. Fetch all the Node dependencies:

		npm install

4. Get the needed libraries through __bower__:

		bower install


## Usage

Start Grunt's __default__ task by simply typing `grunt` in your project's directory.
When you have something production ready just type `grunt build` - you may probably want to do this in
a different terminal than the one where Grunt's default task is running.


## Final Note

Treat this for what it is, a boilerplate which gets you started quickly on your front-end projects by offering
the most needed power tools, so feel free to tweak it to your liking.
